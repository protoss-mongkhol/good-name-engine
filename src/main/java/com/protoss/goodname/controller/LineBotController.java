package com.protoss.goodname.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.LeaveEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.*;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.protoss.goodname.entity.GroupLine;
import com.protoss.goodname.entity.LineLogMessage;
import com.protoss.goodname.service.AppLineBotDataService;
import com.protoss.goodname.service.CheckFullNameService;
import com.protoss.goodname.service.CheckNameService;
import com.protoss.goodname.service.CheckPhoneNumberService;
import com.protoss.goodname.service.GroupLineService;
import com.protoss.goodname.service.LineLogMessageService;
import com.protoss.goodname.service.SpecialService;
import com.protoss.goodname.util.MethodUtil;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Slf4j
@LineMessageHandler
public class LineBotController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private AppLineBotDataService appLineBotDataService;

    @Autowired
    private LineLogMessageService lineLogMessageService;

    @Autowired
    private SpecialService specialService;

    @Autowired
    private GroupLineService groupLineService;



    public static List<Map<String,Object>> LIST_KEYWORD = new ArrayList<>();

    static String displayUserName = "";
    static String groupId = "";

    @EventMapping
    public void handleTextMessage(MessageEvent<TextMessageContent> event) throws IOException {
    	Map<String,String> mapString = new HashMap<String,String>();
    	mapString.put("ชื่อนามสกุล",   "ให้กรอกชื่อ และเว้นวรรคตามด้วยนามสกุล เช่น \"จงรัก ภักดี\"");
    	mapString.put("เบอร์โทรศัพท์", "ให้กรอกเบอร์โทรศัพท์ เช่น \"0830065072\"");
    	mapString.put("ตั้งชื่อลูก", "อาจารย์กำลังฝึกวิชานี้อยู่รอไปก่อนนะ");
    	//mapString.put("ตั้งชื่อลูก", "ให้กรอกวันเกิด,เพศ,นามสกุล เช่น \"วันอาทิคย์,ชาย,เอี่ยมเอิบ\"");
    	
    	String replyToken = event.getReplyToken();
        //handleTextContent(event.getReplyToken(), event, event.getMessage());
    	String message = event.getMessage().getText();
    	
    	if(mapString.get(message) == null){
    		//System.out.println("message="+message);
        	Map<String,String> dataValue = null;
        	if(message.startsWith("0") && message.length() == 10){
        		dataValue = CheckPhoneNumberService.getValueDescription(message);
        		dataValue.put("CASE", "CHECK_PHONE_NUMBER");
        	} else if(message.split(" ").length == 1){//check name
        		dataValue = CheckNameService.getValueDescription(message);
        		dataValue.put("CASE", "CHECK_NAME");
        	} else if(message.split(" ").length > 1){//check first name
        		dataValue = CheckFullNameService.getValueDescription(message);
        		dataValue.put("CASE", "CHECK_FULL_NAME");
        	}
            
            //Map<String,String> dataValue = CheckNameService.valueFullName(event.getMessage().getText());
            //this.replyText(replyToken, dataValue.get("headContent"));

        	switch(String.valueOf(dataValue.get("CASE"))) {
    	        case "CHECK_PHONE_NUMBER":
    	            /* wait for implement*/
    	        	this.replyText(replyToken, message+" เป็นเบอร์เกรด "+dataValue.get("grade")+" ได้คะแนน("+dataValue.get("score")+"/100) ได้ผลรวมเท่ากับ "+dataValue.get("sumResult")+" นั้น"+dataValue.get("sumDetail"));
    	            break;
    	        case "CHECK_NAME":
    	            /* wait for implement*/
    	        	this.replyText(replyToken, message+" มีค่าเป็น"+dataValue.get("headContent")+" "+dataValue.get("contentall"));
    	            break;
    	        case "CHECK_FULL_NAME":
    	            /* wait for implement*/
    	        	this.replyText(replyToken, message+" มีค่าเป็น"+dataValue.get("headContent")+" "+dataValue.get("contentall"));
    	            break;
    	        default:
    	            break;
        	}
    	}else{
    		this.replyText(replyToken, mapString.get(message));
    	}
    	
    	
    	
        

    }

//    private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws IOException {
//        String text = content.getText();
//        String userId = event.getSource().getUserId();
//                if (appLineBotDataService.checkTextMatches(text)) {
//                    if (userId != null) {
//                        lineMessagingClient.getProfile(userId)
//                                .whenComplete((profile, throwable) -> {
//                                    if (throwable != null) {
//                                        this.replyText(replyToken, throwable.getMessage());
//                                        return;
//                                    }
//                                    this.reply(replyToken, Arrays.asList(
//                                            new TextMessage(appLineBotDataService.checkText(text))
//                                    ));
//                                });
//                    }else{
//                        this.replyText(replyToken, "User not found!!");
//                    }
//                }else{
//                    this.replyText(replyToken, "Case not found!!");
//                }
//    }
    private void replyText(@NonNull  String replyToken, @NonNull String message) {
        if(replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }

        if(message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "...";
        }
        this.reply(replyToken, new TextMessage(message));
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        try {
            BotApiResponse response = lineMessagingClient.replyMessage(
                    new ReplyMessage(replyToken, messages)
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

    }


        private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws IOException {
            log.info(" ===- In[handleTextContent] Msg : [{}] -===",content.getText());

            List<Map<String,Object>> listResult = specialService.generateKeyword();

            try{
                String text = content.getText();
                int priority = specialService.findHighPriorityMessage(text);
                String source = event.getSource().toString();
                Map mapLineSource = MethodUtil.findMapDataSourceLine(source);


                switch(priority) {
                    case 0:
                        /* wait for implement*/
                        break;
                    case 1:
                       /* wait for implement*/
                        break;
                    case 3: /* case insert issue msg to DB */

                        /* prepare object for insert*/


                        String userId = event.getSource().getUserId();
                        if(userId != null) {

                            lineMessagingClient.getProfile(userId)
                                    .whenComplete((profile, throwable) -> {
                                            LineLogMessage obj = new LineLogMessage();

                                            obj.setMsg(text);
                                            obj.setPriority(priority);
                                            obj.setCreatedDate(MethodUtil.getCurrentDate());
                                            obj.setGroupLine(String.valueOf(mapLineSource.get("roomId")  == null ? (mapLineSource.get("groupId") == null ? null : String.valueOf(mapLineSource.get("groupId")))  : String.valueOf(mapLineSource.get("roomId"))));
                                            obj.setSender(profile != null  ? profile.getDisplayName() : userId);

                                            /*insert Msg for DB*/
                                            Long idIssue =   lineLogMessageService.insertLineLogMessage(obj);
                                            if(idIssue != null) log.info(" ===- [handleTextContent] Msg : [Save Issue Success] -===");
                                            else log.info(" ===- [handleTextContent] Msg : [Save Issue Fail] -===");

                                    });


                        }


                        break;
                    case 9: /* case insert descript groupLine to DB */
                        GroupLine groupLineObj = new GroupLine();
                        groupLineObj.setRealId(String.valueOf(mapLineSource.get("groupId")));
                        String realDisplayName = MethodUtil.seperateNameGroupLine(text);
                        groupLineObj.setDisplayName(realDisplayName == null ? String.valueOf(mapLineSource.get("userId")) : realDisplayName);
                        groupLineService.insertGroupLine(groupLineObj);
                        break;
                    default:
                        break;
                }

                log.info(" ===- Out[handleTextContent] Msg : [{}] -===");
            }catch(Exception e){
                log.error(" ===- Exception[handleTextContent] Msg : [{}] -===",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }

    }





}
