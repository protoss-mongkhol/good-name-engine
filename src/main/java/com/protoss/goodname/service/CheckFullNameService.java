package com.protoss.goodname.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class CheckFullNameService {
	
	public static Map<String,String> getValueDescription(String fullName) throws IOException {
		
		String fname = fullName.split(" ")[0];
		String lname = fullName.substring(fullName.trim().indexOf(" "), fullName.length()).trim();
		
		ResponseEntity<String> reponseEntity = postWithMapParameter(new RestTemplate(),"http://www.fortunename.com/?stype=shwnm",fname,lname,"sun");
		String content = reponseEntity.getBody();
		String contentA1 = content.substring(content.indexOf("show_numeric_flname_desc"),content.indexOf("</div>", content.indexOf("show_numeric_flname_desc")));
		String[] contentFinal = contentA1.substring(contentA1.indexOf("หมายเลข")).split("</span>&nbsp;<span>");
		
		String headContent = contentFinal[0];
		String contentall  = contentFinal[1].split("</span>")[0];
		//System.out.println(content);
		Map<String,String> mapData = new HashMap<String,String>();
		mapData.put("headContent", headContent);
		mapData.put("contentall", contentall);
		return mapData;
	}

	public static ResponseEntity<String> postWithMapParameter(RestTemplate restTemplate, String urlParam,String fname,String lname,String dateb) {
        String url = urlParam;
        HttpMethod httpMethod = null;

        MediaType mediaType = new MediaType("application", "x-www-form-urlencoded", Charset.forName("windows-874"));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        
        body.add("dateb", dateb);
        body.add("fname", fname);
        body.add("lname", lname);
        body.add("chackans", "เรียบร้อยแล้วกรุณากด");


        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(body, headers);
        if (httpMethod == null) {
            httpMethod = HttpMethod.POST;
        }

        FormHttpMessageConverter converter = new FormHttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(mediaType));

        restTemplate.getMessageConverters().add(converter);
        ResponseEntity<String> reponseEntity = restTemplate.postForEntity(url, entity, String.class);
        return reponseEntity;
    }
}
