package com.protoss.goodname.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class CheckPhoneNumberService {

	public static Map<String,String> getValueDescription(String phoneNumber) throws IOException {
		
		ResponseEntity<String> reponseEntity = postWithMapParameter(new RestTemplate(),"https://www.superbermongkol.com/app/",phoneNumber);
		String content = reponseEntity.getBody();
		String contentA1 = content.substring(content.indexOf("<h1 class=\"green bold\">"),content.indexOf("</h1>", content.indexOf("<h1 class=\"green bold\">")));
		String contentA2 = content.substring(content.indexOf("<h1 class=\"sum-result\">"),content.indexOf("</h1>", content.indexOf("<h1 class=\"sum-result\">")));
		String contentA3 = content.substring(content.indexOf("<div class=\"sum-detail\">"),content.indexOf("</div>", content.indexOf("<div class=\"sum-detail\">")));
		
		String sumResult = contentA2.substring(23);
		String sumDetail = contentA3.substring(53);
		
		Integer sumValue = 0;
		String grade;
		try {
			sumValue = new Integer(contentA1.substring(23,25));
			grade = "F-";
			if(sumValue > 98 ){
				grade = "A++";
			}else if(sumValue > 95){
				grade = "A+";
			}else if(sumValue > 90){
				grade = "A";
			}else if(sumValue > 85){
				grade = "B+";
			}else if(sumValue > 80){
				grade = "B";
			}else if(sumValue > 75){
				grade = "C+";
			}else if(sumValue > 70){
				grade = "C";
			}else if(sumValue > 65){
				grade = "D+";
			}else if(sumValue > 60){
				grade = "D";
			}else if(sumValue > 55){
				grade = "F+";
			}else if(sumValue > 50){
				grade = "F";
			}else {
				grade = "F-";
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			grade = "F-";
		}
		Map<String,String> mapData = new HashMap<String,String>();
		mapData.put("grade", grade);
		mapData.put("score", ""+sumValue);
		mapData.put("sumResult", sumResult);
		mapData.put("sumDetail", sumDetail);
		return mapData;
	}

	public static ResponseEntity<String> postWithMapParameter(RestTemplate restTemplate, String urlParam,String phoneNumber) {
        String url = urlParam;
        HttpMethod httpMethod = null;

        MediaType mediaType = new MediaType("application", "x-www-form-urlencoded", Charset.forName("windows-874"));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        
        body.add("phone", phoneNumber);


        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(body, headers);
        if (httpMethod == null) {
            httpMethod = HttpMethod.POST;
        }

        FormHttpMessageConverter converter = new FormHttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(mediaType));

        restTemplate.getMessageConverters().add(converter);
        ResponseEntity<String> reponseEntity = restTemplate.postForEntity(url, entity, String.class);
        return reponseEntity;
    }
}
