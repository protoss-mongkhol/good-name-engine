package com.protoss.goodname.service;

import com.protoss.goodname.entity.GroupLine;
import com.protoss.goodname.entity.LineLogMessage;
import com.protoss.goodname.repository.GroupLineRepository;
import com.protoss.goodname.repository.LineLogMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupLineServiceImp implements GroupLineService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GroupLineRepository groupLineRepository;


    @Transactional
    @Override
    public Long insertGroupLine(GroupLine obj) {
        try{
            List<GroupLine> groupLines = groupLineRepository.findByRealId(obj.getRealId());

            if(groupLines.size() == 0){
                groupLineRepository.saveAndFlush(obj);
            }else{
                groupLineRepository.deleteAll(groupLines);
                groupLineRepository.flush();
                groupLineRepository.saveAndFlush(obj);
                LOGGER.info(" ===- [insertGroupLine] Msg : [Duplicate Group Id]");
            }

            return obj.getId();
        }catch (Exception e){
            LOGGER.error(" ===- Exception[insertGroupLine] Msg : [{}]",e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
