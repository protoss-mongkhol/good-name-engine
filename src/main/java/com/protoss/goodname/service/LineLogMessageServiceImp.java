package com.protoss.goodname.service;

import com.protoss.goodname.entity.GroupLine;
import com.protoss.goodname.entity.LineLogMessage;
import com.protoss.goodname.entity.MasterDataDetail;
import com.protoss.goodname.repository.GroupLineRepository;
import com.protoss.goodname.repository.LineLogMessageRepository;
import com.protoss.goodname.repository.MasterDataDetailRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class LineLogMessageServiceImp implements LineLogMessageService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LineLogMessageRepository lineLogMessageRepository;

    @Autowired
    private GroupLineRepository groupLineRepository;


    @Transactional
    @Override
    public Long insertLineLogMessage(LineLogMessage obj) {
        try{
            List<GroupLine> groupLineList = groupLineRepository.findByRealId(obj.getGroupLine());
            if(groupLineList.size() > 0){
                obj.setGroupLine(groupLineList.get(0).getDisplayName());
            }
            lineLogMessageRepository.saveAndFlush(obj);
            return obj.getId();
        }catch (Exception e){
                LOGGER.error(" ===- Exception[insertLineLogMessage] Msg : [{}]",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
        }


    }
}
