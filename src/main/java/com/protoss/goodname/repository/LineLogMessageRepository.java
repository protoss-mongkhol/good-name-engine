package com.protoss.goodname.repository;

import com.protoss.goodname.entity.LineLogMessage;
import com.protoss.goodname.entity.MasterData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LineLogMessageRepository extends JpaSpecificationExecutor<LineLogMessage>,
        JpaRepository<LineLogMessage, Long>,
        PagingAndSortingRepository<LineLogMessage, Long> {


}
