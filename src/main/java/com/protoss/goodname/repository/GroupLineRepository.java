package com.protoss.goodname.repository;

import com.protoss.goodname.entity.GroupLine;
import com.protoss.goodname.entity.Priority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupLineRepository extends JpaSpecificationExecutor<GroupLine>,
        JpaRepository<GroupLine, Long>,
        PagingAndSortingRepository<GroupLine, Long> {


    List<GroupLine> findByRealId(@Param("realId")String realId);


}
